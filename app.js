// SETUP
// =============================================================================
require('./config/Config');
// for config use the var global.bots.NAME
global.bots.pjson = require('./package.json');

if (global.bots.config.newRelic.licenseKey) {
    console.info("Monitoring: starting with new relic");
    require('newrelic');
}

// BASIC FRAMEWORKS
// ---------------------
var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var compression = require('compression');
var mongoose = require('mongoose');
var http = require('http');
var util = require('./util');

// BASE SETUP
// =============================================================================
var app = express();
var port = process.env.HTTP_PORT || global.bots.config.port;

// STARTUP INFOS
// =============================================================================
console.log("-----------------------------------------------");
console.log("app: " + global.bots.pjson.name);
console.log("stage: " + global.bots.stageToUse);
console.log("version: " + global.bots.pjson.version);
console.log("port: " + port);
console.log("-----------------------------------------------");

var controller = require('./controller');
var model = require('./model'); // creates connection to mongo

__setupApp();
// CONFIGURATIONS
__setupRoutes();

// start http server
http.createServer(app).listen(port, function () {
    console.log("http:  api server listening on port " + port);
});

function __setupApp() {
    /**
     * BOT
     * must be before bodyParser
     */
    // Messenger
    if (controller.Bot.Messenger.Listener.isEnabled()) {
        app.use('/' + global.bots.config.apiVersion + '/bot/messenger/development', controller.Bot.Messenger.Listener.middleware());
        app.use('/' + global.bots.config.apiVersion + '/bot/messenger/production', controller.Bot.Messenger.Listener.middleware());
    }
    // Kik
    if (controller.Bot.Kik.Listener.isEnabled()) {
        app.use(controller.Bot.Kik.Listener.incoming());
    }
    // Telegram uses polling

    // Skype - use a bodyParser for it
    if (controller.Bot.Skype.Listener.isEnabled()) {
        app.use('/' + global.bots.config.apiVersion + '/bot/skype/development', [bodyParser.json(), controller.Bot.Skype.Listener.getMessagingHandler()]);
        app.use('/' + global.bots.config.apiVersion + '/bot/skype/production', [bodyParser.json(), controller.Bot.Skype.Listener.getMessagingHandler()]);
    }

    app.use(compression());
    // configure app to use bodyParser()
    // this will let us get the data from a POST
    app.use(bodyParser.urlencoded({extended: true, limit: '20mb'}));
    app.use(bodyParser.json({limit: '20mb'}));

    console.log("__setupApp finished");
}

function __setupRoutes() {
    console.log("assign middleware functions");
    // ASSIGN MIDDLEWARE FUNCTIONS FOR EVERY ROUTE
    app.use(function (req, res, next) { // set request start date
        // TODO: do this in a shared middleware
        req.start = Date.now(); // set startdate of the request to calculate the duration later
        next();
    });
    app.use(function (req, res, next) {
        if (req.url != "/status") { // dont want to see the status requests all the time
            console.log(
                "REQUEST: " + req.method + " " + req.url,
                "BODY: " + JSON.stringify(req.body),
                false && !global.bots.config.production ? "HEADER: " + JSON.stringify(req.headers) : ""
            );
        }

        util.Helpers.parseUserAgentFromRequest(req);

        // GO GO GO
        next();
    });

    // ASSIGN ROUTES
    app.use(require('./Router'));
}

/**
 * Gets called if the process will be killed
 * Can do some actions before the process got killed
 */
process.on('SIGTERM', function () {
    console.warn("received SIGTERM");
    console.warn("--------------------------------------------");

    setTimeout(function () {
        process.exit(0);
    }, 200);
});

/**
 * Your process is going to be reloaded
 * You have to close all database/socket.io/* connections
 */
process.on('message', function (msg) {
    console.warn("process.on('message')", msg);
    if (msg == 'shutdown') {
        console.warn('Closing all connections...');

        // You will have x ms to close all connections before
        // the reload mechanism will try to do its job

        setTimeout(function () {
            console.warn('Finished closing connections');
            // This timeout means that all connections have been closed
            // Now we can exit to let the reload mechanism do its job
            process.exit(0);
        }, 1500);
    }
});
