// BASIC FRAMEWORKS
// ---------------------

// MODELS
// ---------------------

// CONTROLLERS
// ---------------------
var util = require("./");

/**
 * send response back to user
 * @param req
 * @param res
 * @param statusCode
 * @param responseJSON
 * @returns {*}
 * @private
 */
function __sendResponse(req, res, statusCode, responseJSON) {
    // set application name
    res.setHeader("application-name", global.bots.pjson.name);
    // set execution duration
    if (req.start) {
        res.setHeader("execution-duration-application", Date.now() - req.start);
    }
    return res.status(statusCode).send(responseJSON);
}

/**
 *
 * @type {{HttpStatus: *, sendError: response.sendError, sendOK: response.sendOK}}
 */
var response = {
    HttpStatus: require('http-status-codes'),
    /**
     * send error response
     * @param req
     * @param res
     * @param errorMessage
     * @param statusCode default:INTERNAL_SERVER_ERROR
     * @returns {*}
     */
    sendError: function sendError(req, res, errorMessage, statusCode) {
        if (!res) return;
        if (!statusCode) statusCode = response.HttpStatus.INTERNAL_SERVER_ERROR;
        if (!errorMessage) errorMessage = response.HttpStatus.getStatusText(statusCode);
        // set error-message
        res.setHeader("error-message", errorMessage);
        // respond now
        return __sendResponse(req, res, statusCode, {success: false, error: errorMessage});
    },
    /**
     * send OK response
     * @param req
     * @param res
     * @param responseJSON
     * @param statusCode default:OK
     * @returns {*}
     */
    sendOK: function sendOK(req, res, responseJSON, statusCode) {
        if (!res) return;
        if (!statusCode) statusCode = response.HttpStatus.OK;
        if (!responseJSON) responseJSON = {};
        responseJSON.success = true;
        // respond now
        return __sendResponse(req, res, statusCode, responseJSON);
    }
};

module.exports = response;