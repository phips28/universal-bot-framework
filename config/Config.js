var config = require('./config.json');

global.bots = {config: {}};

console.log("CONFIG: process.env.STAGE:", process.env.STAGE);

if (process.env.STAGE == undefined) {
    process.env.STAGE = "development"; // DEFAULT stage
}
global.bots.stageToUse = process.env.STAGE;
config = config[process.env.STAGE];
global.bots.config = config;